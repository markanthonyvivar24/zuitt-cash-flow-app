import React, { useState, useEffect } from "react";
import Register from "./pages/register";
import Login from "./pages/login";
import CategoryPage from "./pages/categoryPage";
import { UserProvider } from "./UserContext";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Switch } from "react-router-dom";
import AppNavBar from "./components/AppNavBar";
import AddCategory from "./pages/addCategory";
import Homepage from "./pages/homepage";
import Logout from "./pages/logout";
import AddExpense from "./pages/addExpense";
import Expenses from "./pages/expenses";
import AddIncome from "./pages/addIncome";
import Incomes from "./pages/incomes";

function App() {
  const [user, setUser] = useState({
    email: localStorage.getItem("email"),
    isAdmin: localStorage.getItem("isAdmin") === "true",
  });

  const unsetUser = () => {
    localStorage.clear();
  };
  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />
        <Switch>
          <Route exact path="/" component={Homepage} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/categories" component={CategoryPage} />
          <Route exact path="/addCategory" component={AddCategory} />
          <Route exact path="/addExpense" component={AddExpense} />
          <Route exact path="/allExpenses" component={Expenses} />
          <Route exact path="/addIncome" component={AddIncome} />
          <Route exact path="/allIncomes" component={Incomes} />
          <Route exact path="/logout" component={Logout} />
        </Switch>
      </Router>
    </UserProvider>
  );
}

export default App;
