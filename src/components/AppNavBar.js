import React, { useContext } from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import UserContext from "../UserContext";
import "./AppNavbar.css";

export default function AppNavBar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar className="text-light">
      <Navbar.Brand className="text-light font-weight-bold">
        Zuitt Cash Flow
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          {user.email ? (
            <>
              <NavDropdown title="Category" id="basic-nav-dropdown">
                <NavDropdown.Item
                  className="text-dark"
                  as={NavLink}
                  to="/addCategory"
                >
                  Add Category
                </NavDropdown.Item>
                <NavDropdown.Item
                  className="text-dark"
                  as={NavLink}
                  to="/categories"
                >
                  Categories
                </NavDropdown.Item>
              </NavDropdown>

              <NavDropdown title="Expense" id="basic-nav-dropdown">
                <NavDropdown.Item
                  className="text-dark"
                  as={NavLink}
                  to="/addExpense"
                >
                  Add Entry
                </NavDropdown.Item>
                <NavDropdown.Item
                  className="text-dark"
                  as={NavLink}
                  to="/allExpenses"
                >
                  Expenses
                </NavDropdown.Item>
              </NavDropdown>

              <NavDropdown title="Income" id="basic-nav-dropdown">
                <NavDropdown.Item
                  className="text-dark"
                  as={NavLink}
                  to="/addIncome"
                >
                  Add Entry
                </NavDropdown.Item>
                <NavDropdown.Item
                  className="text-dark"
                  as={NavLink}
                  to="/allIncomes"
                >
                  Incomes
                </NavDropdown.Item>
              </NavDropdown>

              <NavDropdown title={user.email} id="basic-nav-dropdown">
                <NavDropdown.Item
                  className="text-dark"
                  as={NavLink}
                  to="/logout"
                >
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
            </>
          ) : (
            <>
              <Nav.Link className="text-light" as={NavLink} to="/">
                Home
              </Nav.Link>

              <Nav.Link className="text-light" as={NavLink} to="/login">
                Login
              </Nav.Link>

              <Nav.Link className="text-light" as={NavLink} to="/register">
                Register
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
