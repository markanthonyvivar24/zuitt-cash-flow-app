import React from "react";
import { Card, Container, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import "./Categories.css";

export default function Categories({ props }) {
  const { name, type, _id } = props;

  const deleteCategory = (e) => {
    console.log(_id);
    e.preventDefault();

    fetch(`https://salty-ocean-60112.herokuapp.com/api/categories/${_id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => {
        res.json();
      })
      .then((data) => {});

    Swal.fire({
      icon: "success",
      title: "Category has been deleted",
      showConfirmButton: true,
    });
  };

  return (
    <>
      <Container>
        <Card className="cardHighlight my-5">
          <Card.Body className="card-body">
            <Card.Text>
              <h3 className="mb-3 text-light">Name: {name}</h3>
              <h3 className="text-light">Type: {type}</h3>
              <div className="text-center mt-5  ">
                {/* <Button
                  variant="outline-light"
                  className="btn-edit"
                  onClick={(e) => deleteCategory(e)}
                >
                  Edit
                </Button> */}

                <Button
                  variant="danger"
                  className="btn-delete ml-2"
                  onClick={(e) => deleteCategory(e)}
                >
                  Delete
                </Button>
              </div>
            </Card.Text>
          </Card.Body>
        </Card>
      </Container>
    </>
  );
}
