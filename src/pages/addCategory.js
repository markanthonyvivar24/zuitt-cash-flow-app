import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import "./addCategoryPage.css";

export default function AddCategory() {
  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [isActive, setIsActive] = useState(true);

  useEffect(() => {
    if (name === "" || type === "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  });

  const addCategory = (e) => {
    e.preventDefault();

    fetch("https://salty-ocean-60112.herokuapp.com/api/categories", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        type: type,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.error) {
          Swal.fire({
            icon: "error",
            title: "Category name does exist!",
            text: "Pls provide another name.",
          });
        } else {
          Swal.fire("Category has been added", "", "success");
        }
      });

    setName("");
    setType("");
  };

  return (
    <>
      <h1 className="text-center text-light category-text font-weight-bold mt-5">
        Add Your Category
      </h1>
      <div className="addCategory">
        <Form onSubmit={(e) => addCategory(e)}>
          <Form.Group>
            <Form.Label className="text-light font-weight-bold">
              Name
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="Name"
              value={name}
              onChange={(e) => setName(e.target.value)}
            ></Form.Control>
          </Form.Group>

          <Form.Group>
            <Form.Label className="text-light font-weight-bold">
              Type
            </Form.Label>
            <Form.Control
              as="select"
              type="text"
              placeholder="Type"
              value={type}
              onChange={(e) => setType(e.target.value)}
            >
              <option>Choose Type</option>
              <option>Expense</option>
              <option>Income</option>
            </Form.Control>
          </Form.Group>

          <div className="text-center">
            <Button
              type="submit"
              variant="light"
              className="mt-5"
              disabled={isActive}
            >
              Add Category
            </Button>
          </div>
        </Form>
      </div>
    </>
  );
}
