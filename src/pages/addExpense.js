import { Form, Button } from "react-bootstrap";
import React, { useState, useEffect } from "react";
import Swal from "sweetalert2";

import "./addExpense.css";

export default function AddExpenses() {
  const [category, setCategory] = useState("");
  const [amount, setAmount] = useState("");
  const [isActive, setIsActive] = useState(true);
  const [categoryFromDB, setCategoryFromDB] = useState([]);

  useEffect(() => {
    if (amount === "" || category === "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [amount, category]);

  useEffect(() => {
    fetch("https://salty-ocean-60112.herokuapp.com/api/categories", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCategoryFromDB(
          data.filter((category) => category.type === "Expense")
        );
      });
  }, []);

  const addEntry = (e) => {
    e.preventDefault();
    fetch("https://salty-ocean-60112.herokuapp.com/api/entries", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        category: category,
        amount: amount,
        type: "Expense",
      }),
    })
      .then((res) => res.json())
      .then((data) => Swal.fire("Expense has been added", "", "success"));

    setCategory("");
    setAmount("");
  };

  const allCategoryName = categoryFromDB.map((category) => (
    <option>{category.name}</option>
  ));

  return (
    <>
      <h1 className="text-light text-center mt-5 font-weight-bold expense-text">
        Add Your Expense
      </h1>
      <div className="expense-form">
        <Form onSubmit={(e) => addEntry(e)}>
          <Form.Group>
            <Form.Label className="text-light font-weight-bold">
              Category
            </Form.Label>
            <Form.Control
              as="select"
              value={category}
              onChange={(e) => setCategory(e.target.value)}
            >
              <option>Choose</option>
              {allCategoryName}
            </Form.Control>

            <Form.Label className="text-light font-weight-bold">
              Amount
            </Form.Label>
            <Form.Control
              type="number"
              placeholder="Amount"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
            ></Form.Control>

            <div className="text-center mt-5">
              <Button variant="light" disabled={isActive} type="submit">
                Add Expense
              </Button>
            </div>
          </Form.Group>
        </Form>
      </div>
    </>
  );
}
