import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import "./addIncome.css";

export default function AddIncome() {
  const [category, setCategory] = useState("");
  const [amount, setAmount] = useState("");
  const [isActive, setIsActive] = useState(true);
  const [categoryFromDB, setCategoryFromDB] = useState([]);

  useEffect(() => {
    if (amount === "" || category === "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [amount, category]);

  useEffect(() => {
    fetch("https://salty-ocean-60112.herokuapp.com/api/categories", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        setCategoryFromDB(
          data.filter((category) => category.type === "Income")
        );
      });
  }, []);

  // console.log(categoryFromDB);

  const allCategoryName = categoryFromDB.map((category) => (
    <option>{category.name}</option>
  ));

  const addEntry = (e) => {
    e.preventDefault();
    fetch("https://salty-ocean-60112.herokuapp.com/api/entries", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        category: category,
        amount: amount,
        type: "Income",
      }),
    })
      .then((res) => res.json())
      .then((data) => Swal.fire("Income has been added", "", "success"));

    setCategory("");
    setAmount("");
  };

  return (
    <>
      <h1 className="text-center mt-5 font-weight-bold text-light income-text">
        Add Income
      </h1>

      <div className="income-form">
        <Form onSubmit={(e) => addEntry(e)}>
          <Form.Group>
            <Form.Label className="text-light font-weight-bold">
              Category
            </Form.Label>
            <Form.Control
              as="select"
              value={category}
              onChange={(e) => setCategory(e.target.value)}
            >
              <option>Choose</option>
              {allCategoryName}
            </Form.Control>

            <Form.Label className="text-light font-weight-bold">
              Amount
            </Form.Label>

            <Form.Control
              type="number"
              placeholder="Amount"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
            />

            <div className="text-center mt-5">
              <Button variant="light" type="submit" disabled={isActive}>
                Add Income
              </Button>
            </div>
          </Form.Group>
        </Form>
      </div>
    </>
  );
}
