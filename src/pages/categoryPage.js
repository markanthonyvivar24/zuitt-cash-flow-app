import React, { useState, useEffect } from "react";
import Categories from "../components/Categories";

export default function CategoryPage() {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    fetch("https://salty-ocean-60112.herokuapp.com/api/categories", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCategories(data);
      });
  }, [categories]);

  let allCategories = categories.map((category) => {
    return <Categories key={category._id} props={category} />;
  });

  console.log(categories);

  return (
    <>
      <h1 className="text-light text-center mt-5 font-weight-bold registration-text">
        Categories
      </h1>

      {allCategories}
    </>
  );
}
