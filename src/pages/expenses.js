import React, { useEffect, useState } from "react";
import Expenses2 from "./expenses2";
import "./expenses.css";
import "./table.css";

export default function Expenses() {
  const [expenses, setExpenses] = useState([]);
  const [total, setTotal] = useState([]);

  useEffect(() => {
    fetch("https://salty-ocean-60112.herokuapp.com/api/entries/expenses", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setExpenses(data);
        setTotal(
          data.map((data) => data.amount).reduce((acc, value) => acc + value, 0)
        );
      });
  }, [expenses]);

  console.log(expenses);
  let allExpenses = expenses.map((expense) => {
    return <Expenses2 props={expense} />;
  });

  return (
    <>
      <h1 className="text-light text-center font-weight-bold mt-5 expenses-text">
        Your Expenses
      </h1>

      <div className="table text-light">
        <h3>Total Expense: </h3>
        <h3>Php {total}</h3>
      </div>

      {allExpenses}
    </>
  );
}
