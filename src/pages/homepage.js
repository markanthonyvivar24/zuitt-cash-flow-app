import React from "react";
import { Jumbotron, Container } from "react-bootstrap";
import "./homepage.css";

export default function Homepage() {
  return (
    <>
      <div className="banner text-light">
        <h1 className="text1">Welcome To Zuitt Cash Flow App</h1>
        <h2 className="text2">
          Budgeting has only one rule: Don't go over budget
        </h2>
      </div>
      <div className="cont"></div>
    </>
  );
}
