import React, { useState, useEffect } from "react";
import Incomes2 from "./incomes2";
import "./incomes.css";
import "./table.css";

export default function Incomes() {
  const [incomes, setIncomes] = useState([]);
  const [total, setTotal] = useState([]);

  useEffect(() => {
    fetch("https://salty-ocean-60112.herokuapp.com/api/entries/income", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setIncomes(data);
        setTotal(
          data.map((data) => data.amount).reduce((acc, value) => acc + value, 0)
        );
      });
  }, [incomes]);

  // console.log(incomes);
  // console.log(total);

  let allIncomes = incomes.map((income) => {
    return <Incomes2 props={income} />;
  });

  return (
    <>
      <h1 className="text-center font-weight-bold my-5 text-light incomes-text">
        Your Incomes
      </h1>

      <div className="table text-light">
        <h3>Total Income: </h3>
        <h3>Php {total}</h3>
      </div>

      {allIncomes}
    </>
  );
}
