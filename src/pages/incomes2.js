import React, { useState } from "react";
import { Container, Card, Button, Form, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Incomes2({ props }) {
  //   console.log(props);
  const { category, amount, _id } = props;
  const [show, setShow] = useState(false);
  const [name, setName] = useState("");
  const [amount2, setAmount2] = useState("");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const deleteEntry = (e) => {
    console.log(_id);
    e.preventDefault();

    fetch(`https://salty-ocean-60112.herokuapp.com/api/entries/${_id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {});
    Swal.fire({
      icon: "success",
      title: "Income has been deleted",
      showConfirmButton: true,
    });
  };

  const editIncome = (e) => {
    e.preventDefault();
    fetch(`https://salty-ocean-60112.herokuapp.com/api/entries/${_id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        category: name,
        amount: amount2,
        type: "Income",
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        handleClose();
      });
  };
  return (
    <Container>
      <Card className="cardHighlight my-5">
        <Card.Body className="card-body">
          <Card.Text>
            <h3 className="mb-3 text-light">Category: {category} </h3>
            <h3 className="text-light">Amount: Php {amount} </h3>
            <div className="text-center mt-5">
              <Button
                variant="primary"
                className="btn-edit"
                onClick={handleShow}
              >
                Edit
              </Button>

              <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title>Edit your income</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <Form>
                    <Form.Group>
                      <Form.Label className="text-light font-weight-bold">
                        Category
                      </Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                      ></Form.Control>

                      <Form.Label className="text-light font-weight-bold">
                        Amount
                      </Form.Label>
                      <Form.Control
                        type="number"
                        placeholder="Amount"
                        value={amount2}
                        onChange={(e) => setAmount2(e.target.value)}
                      ></Form.Control>
                    </Form.Group>
                  </Form>
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="primary" onClick={editIncome}>
                    Save Changes
                  </Button>

                  <Button variant="secondary" onClick={handleClose}>
                    Close
                  </Button>
                </Modal.Footer>
              </Modal>

              <Button
                variant="danger"
                className="btn-delete ml-2"
                onClick={(e) => deleteEntry(e)}
              >
                Delete
              </Button>
            </div>
          </Card.Text>
        </Card.Body>
      </Card>
    </Container>
  );
}
