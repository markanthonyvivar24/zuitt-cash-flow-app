import React, { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import UserContext from "../UserContext";
import { Redirect } from "react-router-dom";
import Swal from "sweetalert2";
import "./login.css";

export default function Login() {
  const { user, setUser } = useContext(UserContext);
  //   console.log(user);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(true);
  const [willRedirect, setWillRedirect] = useState(false);

  useEffect(() => {
    if (email === "" || password === "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  const loginUser = (e) => {
    e.preventDefault();

    fetch("https://salty-ocean-60112.herokuapp.com/api/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        if (data.accessToken) {
          localStorage.setItem("token", data.accessToken);

          fetch("https://salty-ocean-60112.herokuapp.com/api/users", {
            headers: {
              Authorization: `Bearer ${data.accessToken}`,
            },
          })
            .then((res) => res.json())
            .then((data) => {
              // console.log(data);

              localStorage.setItem("email", data.email);
              localStorage.setItem("isAdmin", data.isAdmin);

              setWillRedirect(true);

              setUser({
                email: data.email,
                isAdmin: data.isAdmin,
              });
              Swal.fire(
                "Logged In Successfully",
                "Thank you for logging in.",
                "success"
              );
            });
        } else {
          Swal.fire({
            icon: "error",
            title: "Login Failed!",
            text: "Make sure you are registered. Email and password should be matched.",
          });
        }
      });
  };
  return willRedirect ? (
    <Redirect to="/categories" />
  ) : (
    <div>
      <h1 className="text-light text-center font-weight-bold mt-5 login-text">
        Login Form
      </h1>
      <div className="login-form">
        <Form onSubmit={(e) => loginUser(e)}>
          <Form.Group>
            <Form.Label className="text-light font-weight-bold">
              Email
            </Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter your email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group>
            <Form.Label className="text-light font-weight-bold">
              Password
            </Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter your password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>

          <div className="text-center mt-5">
            <Button variant="light" size="lg" type="submit" disabled={isActive}>
              Login
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
}
