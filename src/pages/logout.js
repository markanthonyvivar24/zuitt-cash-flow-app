import React, { useContext } from "react";
import UserContext from "../UserContext";
import { Redirect } from "react-router-dom";
import Swal from "sweetalert2";

export default function Logout() {
  const { setUser, unsetUser } = useContext(UserContext);
  unsetUser();

  setUser({
    email: null,
    isAdmin: null,
  });
  Swal.fire("Logged Out Successfully");
  return <Redirect to="/" />;
}
