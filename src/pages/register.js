import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";

import Swal from "sweetalert2";
import "./register.css";

export default function Register() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [isActive, setIsActive] = useState(true);

  const history = useHistory();

  useEffect(() => {
    if (
      firstName === "" ||
      lastName === "" ||
      email === "" ||
      password === "" ||
      confirmPassword === "" ||
      password.length < 8 ||
      password !== confirmPassword
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, password, confirmPassword]);

  const registerUser = (e) => {
    e.preventDefault();
    fetch("https://salty-ocean-60112.herokuapp.com/api/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
        confirmPassword: confirmPassword,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.email === email) {
          history.push("/login");

          Swal.fire({
            icon: "success",
            title: "Registered Successfully ",
            showConfirmButton: true,
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Email does exist!",
            text: "Pls provide another email.",
          });
        }
      });
    setFirstName("");
    setLastName("");
    setEmail("");
    setPassword("");
    setConfirmPassword("");
  };

  return (
    <>
      <h1 className="text-light text-center mt-5 font-weight-bold registration-text">
        Registration Form
      </h1>
      <div className="registration-form">
        <Form onSubmit={(e) => registerUser(e)}>
          <Form.Group>
            <Form.Label className="text-light font-weight-bold">
              First Name
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter your first name"
              value={firstName}
              onChange={(e) => {
                setFirstName(e.target.value);
              }}
              required
            ></Form.Control>
          </Form.Group>

          <Form.Group>
            <Form.Label className="text-light font-weight-bold">
              Last Name
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter your last name"
              value={lastName}
              onChange={(e) => {
                setLastName(e.target.value);
              }}
              required
            ></Form.Control>
          </Form.Group>

          <Form.Group>
            <Form.Label className="text-light font-weight-bold">
              Email
            </Form.Label>
            <div></div>
            <Form.Control
              type="email"
              placeholder="Enter your email"
              value={email}
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              required
            ></Form.Control>
          </Form.Group>

          <Form.Group>
            <Form.Label className="text-light font-weight-bold">
              Password
            </Form.Label>
            <span className="validation-text">
              {password.length >= 8 && password === confirmPassword
                ? ""
                : "( Should be equal or greater than 8 characters * )"}
            </span>
            <Form.Control
              type="password"
              placeholder="Enter your password"
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              required
            ></Form.Control>
          </Form.Group>

          <Form.Group>
            <Form.Label className="text-light font-weight-bold">
              Confirm Password
            </Form.Label>
            <span className="validation-text">
              {confirmPassword.length >= 8 && password === confirmPassword
                ? ""
                : "( Should be equal or greater than 8 characters * )"}
            </span>
            <Form.Control
              type="password"
              placeholder="Enter your confirm password"
              value={confirmPassword}
              onChange={(e) => {
                setConfirmPassword(e.target.value);
              }}
              required
            ></Form.Control>
          </Form.Group>

          <div className="text-center mt-5">
            <Button variant="light" size="lg" type="submit" disabled={isActive}>
              Register
            </Button>
          </div>
        </Form>
      </div>
      <div className="registration-footer"></div>
    </>
  );
}
